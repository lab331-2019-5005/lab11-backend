package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;
import se331.lab.rest.service.StudentAnotherService;
@Controller
@Slf4j
public class CourseAnotherController {
    @Autowired
    CourseAnotherService courseAnotherService;
    @GetMapping("/CourseWhichStudentEnrolledMoreThan/{enrolledCourse}")
    public ResponseEntity getStudentByNameContains(@PathVariable double enrolledCourse) {
        log.info("the course another controller is called");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(this.courseAnotherService.getCourseWhichStudentEnrolledMoreThan(enrolledCourse)));
    }
}

